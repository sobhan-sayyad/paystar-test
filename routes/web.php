<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->group( function () {
    Route::get('/', function () {
        return redirect()->route('dashboard');
    });
    Route::prefix('/user')->group(function () {
        Route::get('/dashboard', [UserController::class, 'dashboard'])->name('dashboard');

        Route::prefix('/orders')->group(function () {
            Route::get('/', [OrderController::class, 'all'])->name('orders');
            Route::get('/create', [OrderController::class, 'create'])->name('orders.create');
            Route::post('/create', [OrderController::class, 'store']);
            Route::get('/{order}', [OrderController::class, 'show'])->name('orders.show')->middleware('order.user');
        });
        Route::prefix('/payment')->group(function () {
            Route::post('/{order}', [PaymentController::class, 'startPayment'])->name('orders.payment')->middleware('order.user');
            Route::get('/{order}/verify', [PaymentController::class, 'verifyPayment'])->name('orders.payment-verify');
        });
    });
});

Route::get('/login', [AuthController::class, 'loginForm'])->name('login');
Route::post('/login', [AuthController::class, 'login']);
Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
