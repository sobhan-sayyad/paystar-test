@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">ورود به حساب کاربری</div>
                    <div class="card-body">
                        <form action="{{ route('login') }}" method="POST">
                            @csrf
                            <div class="mb-3">
                                @error('credentials')
                                <div class="alert-danger">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label">ایمیل</label>
                                <input type="email" class="form-control @error('credentials') is-invalid @enderror" id="email" name="email" placeholder="youremail@address.com" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            </div>
                            <div class="mb-3">
                                <label for="password" class="form-label">گذرواژه</label>
                                <input type="password" class="form-control @error('credentials') is-invalid @enderror" id="password" name="password" placeholder="********" autocomplete="current-password" required>
                            </div>
                            <div class="d-grid">
                                <button class="btn btn-primary">ورود</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
