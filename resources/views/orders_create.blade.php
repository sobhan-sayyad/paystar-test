@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">ثبت سفارش جدید</div>
                    <div class="card-body">
                        <form action="{{ route('orders.create') }}" method="POST">
                            @csrf
                            <div class="mb-3">
                                @error('description')
                                <div class="alert-danger">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @enderror
                                <label for="description" class="form-label">توضیحات سفارش</label>
                                <input type="text" class="form-control @error('description') is-invalid @enderror" id="description" name="description" placeholder="خدمت مورد نیاز شما ..." value="{{ old('description') }}" required>
                            </div>
                            <div class="mb-3">
                                @error('amount')
                                <div class="alert-danger">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @enderror
                                <label for="amount" class="form-label">مبلغ پرداختی</label>
                                <input type="number" class="form-control @error('amount') is-invalid @enderror" id="amount" name="amount" placeholder="10000000" required>
                            </div>
                            <div class="d-grid">
                                <button class="btn btn-primary">ثبت سفارش</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
