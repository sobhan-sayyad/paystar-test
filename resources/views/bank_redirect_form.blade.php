@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                در حال انتقال به درگاه در یک ثانیه
            </div>
        </div>
        <div id="my-form">
            <form method="{{ $form['method'] }}" action="{{ $form['action'] }}" id="payment-form">
                @csrf
                @foreach($form['parameters'] as $key => $value)
                    <input type="text" name="{{ $key }}" value="{{ $value }}" hidden>
                @endforeach
                <button type="submit" class="btn btn-primary">هم اکنون برو</button>
            </form>
        </div>
    </div>
@endsection

@push('script')
    <script>
        // Wait for 3 seconds before submitting the form
        setTimeout(function() {
            document.getElementById('payment-form').submit().disabled = true;
        }, 1000);
    </script>
@endpush
