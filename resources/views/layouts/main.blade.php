<!doctype html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>پی استار</title>

    {{--Bootstrap CSS--}}
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
</head>
<body class="p-5">
    @yield('content')
</body>
<footer>
    {{--Bootstrap JS--}}
    <link rel="stylesheet" href="{{ asset('js/bootstrap.min.js') }}">
    @stack('script')
</footer>
</html>
