@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        میزکار
                    </div>
                    <div class="card-body">
                        <h5>سلام {{ $user->name }} خوش آمدید </h5>
                        <p>شما با ایمیل {{ $user->email }} وارد شده اید. </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-6">
                <a href="{{ route('orders') }}" class="btn btn-primary btn-block">سفارشات</a>
            </div>
            <div class="col-md-6">
                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                    @csrf
                    <button class="btn btn-danger btn-block" type="submit">خروج</button>
                </form>
            </div>
        </div>
    </div>
@endsection
