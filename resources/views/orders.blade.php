@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row mb-3">
            <div class="col-md-12">
                <a href="{{ route('orders.create') }}" class="btn btn-success">سفارش جدید</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">سفارشات من</div>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>شماره سفارش</th>
                                    <th>توضیحات</th>
                                    <th>وضعیت</th>
                                    <th>مبلغ(ریال)</th>
                                    <th>جزئیات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($orders as $order)
                                    <tr>
                                        <td>{{ $order->id }}</td>
                                        <td>{{ $order->description }}</td>
                                        <td>
                                            @switch($order->status)
                                                @case(\App\Models\Order::STATUS_INIT)
                                                    ثبت شده(در انتظار پرداخت)
                                                    @break
                                                @case(\App\Models\Order::STATUS_PAYED)
                                                    پرداخت شده(در حال اجرا)
                                                    @break
                                                @case(\App\Models\Order::STATUS_FINISHED)
                                                    خاتمه یافته
                                                    @break
                                                @case(\App\Models\Order::STATUS_CANCELED)
                                                    لغو شده
                                                    @break
                                            @endswitch
                                        </td>
                                        <td>{{ number_format($order->amount) }}</td>
                                        <td>
                                            <a href="{{ route('orders.show', $order->id) }}" class="btn btn-primary">جزئیات</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
