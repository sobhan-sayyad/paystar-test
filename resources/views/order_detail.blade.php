@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        جزئیات سفارش
                    </div>
                    <div class="card-body">
                        <ol>
                            <li><p>سفارش شماره: {{ $order->id }}</p></li>
                            <li><p>توضیحات سفارش: {{ $order->description }}</p></li>
                            <li><p>مبلغ سفارش: {{ number_format($order->amount) }} ریال</p></li>
                            @if($order->status == \App\Models\Order::STATUS_INIT)
                            <li><p class="alert-danger">شماره کارت معرفی شده کاربر برای واریز وجه <br><strong>{{ $order->user->verified_card->card_number }}<br></strong> می‌باشد و پرداخت فقط با این شماره کارت‌ مورد قبول است.</p></li>
                            @elseif($order->status == \App\Models\Order::STATUS_PAYED)
                                <li><p class="alert-success">سفارش در حال انجام است.</p></li>
                            @elseif($order->status == \App\Models\Order::STATUS_FINISHED)
                                <li><p class="alert-success">سفارش با موفقیت خاتمه یافته است.</p></li>
                            @elseif($order->status == \App\Models\Order::STATUS_CANCELED)
                                <li><p class="alert-danger">سفارش کنسل شده است.</p></li>
                            @endif
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        @if($order->status == \App\Models\Order::STATUS_INIT)
        <div class="card">
            <div class="card-header">پرداخت سفارش</div>
            <div class="card-body">
                <form action="{{ route('orders.payment', $order->id) }}" method="POST">
                    @csrf
                    <div class="mb-3">
                        @error('gateway')
                        <div class="alert-danger">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <input type="radio" class="form-check-input @error('gateway') is-invalid @enderror" id="gateway" name="gateway" placeholder="youremail@address.com" value="paystar" required checked>
                        <label for="gateway" class="form-check-label">درگاه پی‌استار</label>
                    </div>
                    <div class="d-grid">
                        <button class="btn btn-primary">پرداخت</button>
                    </div>
                </form>
            </div>
        </div>
        @endif
    </div>
@endsection
