@extends('layouts.main')

@section('content')
    <div class="container">
        <h1>نتیجه تراکنش</h1>
        <div class="row">
            @if($result)
            <div class="col-md-6 alert-success">
                <h2>تراکنش موفق</h2>
                <p class="lead">سفارش شما در حال اجرا می باشد.</p>
            </div>
            @else
                <div class="col-md-6 alert-danger">
                    <h2>تراکنش ناموفق</h2>
                    <p class="lead">در صورت کسر شدن وجه از حساب شما، مبلغ طی ۷۲ ساعت آینده به حساب شما باز می گردد.</p>
                </div>
            @endif
        </div>
        <div class="row mt-4">
            <div class="col-md-12 text-center">
                <a href="{{ route('orders.show', $orderId) }}" class="btn btn-primary">بازگشت به سفارش</a>
            </div>
        </div>
    </div>
@endsection
