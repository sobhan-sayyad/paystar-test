<?php

namespace App\Exceptions;

use Exception;

class WrongGatewayException extends Exception
{
    protected $message = 'درگاه پرداخت نامعتبر است.';
}
