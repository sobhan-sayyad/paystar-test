<?php

namespace App\Exceptions;

use Exception;

class NoPaymentToVerifyException extends Exception
{
    protected $message = 'سفارش فرایند پرداخت معتبری ندارد';
}
