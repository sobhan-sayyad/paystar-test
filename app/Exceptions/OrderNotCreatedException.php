<?php

namespace App\Exceptions;

use Exception;

class OrderNotCreatedException extends Exception
{
    protected $message = 'مشکلی در ایجاد سفارش شما به وجود آمده است.';
}
