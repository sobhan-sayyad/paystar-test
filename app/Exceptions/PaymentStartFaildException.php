<?php

namespace App\Exceptions;

use Exception;

class PaymentStartFaildException extends Exception
{
    protected $message = 'مشکلی در برقراری ارتباط با درگاه پیش آمده است.';
}
