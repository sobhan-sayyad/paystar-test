<?php

namespace App\Http\Controllers;

use App\Classes\Payment;
use App\Exceptions\NoPaymentToVerifyException;
use App\Models\Payment as PaymentModel;
use App\Classes\PaystarPayment;
use App\Exceptions\PaymentStartFaildException;
use App\Exceptions\WrongGatewayException;
use App\Http\Requests\PaymentRequest;
use App\Models\Order;
use App\Services\PaymentService;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * @throws WrongGatewayException
     */
    public function startPayment(PaymentRequest $request, Order $order)
    {
        try {
            $gateway = $request->validated()['gateway'];

            //New gateways strategies should be added to this switch case
            switch ($gateway) {
                case 'paystar':
                    $paymentStrategy = new PaystarPayment();
                    break;
                default:
                    throw new WrongGatewayException();
            }

            $payment = new Payment($paymentStrategy);
            $result = $payment->start($order);
            return view('bank_redirect_form', ['form' => $result]);
        } catch (PaymentStartFaildException|WrongGatewayException $exception) {
            return redirect()->back()->withErrors(['gateway' => $exception->getMessage()]);
        } catch (\Exception $e) {
            abort(500);
        }
    }

    /**
     * @throws WrongGatewayException
     * @throws NoPaymentToVerifyException
     */
    public function verifyPayment(Request $request, Order $order)
    {
        try {
            $payment = PaymentModel::where([
                'order_id' => $order->id,
                'status'   => PaymentModel::STATUS_INIT,
            ])->latest()->first();
            if (empty($payment)) {
                throw new NoPaymentToVerifyException();
            }
            $gateway = $payment->gateway;

            switch ($gateway) {
                case 'paystar':
                    $paymentStrategy = new PaystarPayment();
                    break;
                default:
                    throw new WrongGatewayException();
            }

            $verifyPayment = new Payment($paymentStrategy);
            $result = $verifyPayment->verify($payment);
            return view('payment_result', ['result' => $result, 'orderId' => $order->id]);
        } catch (WrongGatewayException|NoPaymentToVerifyException $exception) {
            return redirect()->route('orders.show', $order->id)->withErrors(['gateway' => $exception->getMessage()]);
        } catch (\Exception $e) {
            abort(500);
        }
    }
}
