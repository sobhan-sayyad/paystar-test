<?php

namespace App\Http\Controllers;

use App\Exceptions\OrderNotCreatedException;
use App\Http\Requests\CreateOrderRequest;
use App\Models\Order;
use App\Services\OrderService;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class OrderController extends Controller
{
    /**
     * @param OrderService $orderService
     */
    public function __construct(private OrderService $orderService)
    {
        //
    }

    /**
     * @return Renderable
     */
    public function all(): Renderable
    {
        return view('orders', ['orders' => $this->orderService->getAllUserOrders()]);
    }

    public function show(Order $order): Renderable
    {
        return view('order_detail', ['order' => $order]);
    }

    /**
     * @return Renderable
     */
    public function create(): Renderable
    {
        return view('orders_create');
    }

    /**
     * @param CreateOrderRequest $request
     * @return RedirectResponse|Renderable
     */
    public function store(CreateOrderRequest $request): Renderable|RedirectResponse
    {
        try {
            $result = $this->orderService->createNewOrder($request->validated());
            return redirect()->route('orders.show', $result['id']);
        } catch (OrderNotCreatedException $exception) {
            return view('errors', ['errors' => [$exception->getMessage()]]);
        } catch (\Exception $exception) {
            abort(500);
        }
    }

    public function payment(Order $order)
    {
        dd(1);
    }
}
