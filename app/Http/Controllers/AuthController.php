<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Services\AuthService;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * @param AuthService $authService
     */
    public function __construct(private AuthService $authService)
    {
        //
    }

    /**
     * @return Renderable|RedirectResponse
     */
    public function loginForm(): Renderable|RedirectResponse
    {
        if (auth()->user()) {
            return redirect()->route('dashboard');
        }
        return view('login');
    }

    /**
     * @param LoginRequest $request
     * @return RedirectResponse
     */
    public function login(LoginRequest $request): RedirectResponse
    {
        $credentials = $request->validated();

        $login = $this->authService->login($credentials);
        if ($login['status'] == 'success') {
            return redirect()->route('dashboard');
        }
        return redirect()->back()->onlyInput('email')->withErrors(['credentials' => $login['message']]);
    }

    /**
     * @return RedirectResponse
     */
    public function logout(): RedirectResponse
    {
        $this->authService->logout();
        return redirect()->route('login');
    }
}
