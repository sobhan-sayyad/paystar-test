<?php

namespace App\Console\Commands;

use App\Models\CreditCard;
use App\Models\Order;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class InsertInitialUserInfoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert:initial-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'insert initial data in to the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->ask('Please enter your name:');
        $email = $this->ask('Please enter your email address:');
        $password = 'password';
        $passwordConfirm = 'password confirm';
        $i = 0;
        while ($password != $passwordConfirm) {
            if ($i != 0) {
                echo 'Your passwords does not match enter them more carefully';
                echo PHP_EOL;
            }
            $password = $this->secret('Enter your password:');
            $passwordConfirm = $this->secret('Enter your password again to confirm:');
            $i++;
        }
        if ($i > 3) {
            echo 'It really took you ' . $i . 'attempts to type in a password and confirm it? :/ ';
            echo PHP_EOL;
        }
        $hashedPassword = Hash::make($password);

        $cardNumber = $this->ask('Enter a real card number which will be used for payments:');

        $user = User::create([
            'name'              => $name,
            'email'             => $email,
            'email_verified_at' => now(),
            'password'          => $hashedPassword,
        ]);

        CreditCard::create([
            'user_id'     => $user->id,
            'card_number' => $cardNumber,
            'status'      => CreditCard::STATUS_VERIFIED,
        ]);

        Order::create([
            'user_id'     => $user->id,
            'status'      => Order::STATUS_INIT,
            'amount'      => 5000,
            'description' => 'سفارش تست درگاه پی‌استار',
        ]);

        echo 'Your testing profile is ready';
        echo PHP_EOL;
        return 0;
    }
}
