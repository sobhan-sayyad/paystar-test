<?php
namespace App\Services;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthService
{
    public function login($credentials)
    {
        if (Auth::attempt($credentials)) {
            $user = User::where('email', $credentials['email'])->first();
            return [
                'status' => 'success',
            ];
        }
        return [
            'status'  => 'error',
            'message' => 'اطلاعات وارد شده اشتباه است.'
        ];
    }

    public function logout( )
    {
        Auth::guard('web')->logout();
        return true;
    }
}
