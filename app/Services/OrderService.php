<?php

namespace App\Services;

use App\Exceptions\OrderNotCreatedException;
use App\Models\Order;

class OrderService
{
    /**
     * @return mixed
     */
    public function getAllUserOrders()
    {
        return Order::where('user_id', auth()->id())->orderBy('created_at', 'DESC')->get();
    }

    /**
     * @param array $orderRequest
     * @return array
     * @throws OrderNotCreatedException
     */
    public function createNewOrder(array $orderRequest): array
    {
        $orderData = [
            'user_id'     => auth()->id(),
            'status'      => Order::STATUS_INIT,
            'description' => $orderRequest['description'],
            'amount'      => $orderRequest['amount'],
        ];

        $order = Order::create($orderData);

        //throw exception if order was not created
        $order ?? throw new OrderNotCreatedException();

        return $order->toArray();
    }
}
