<?php

namespace App\Classes\Interfaces;

use App\Models\Order;
use App\Models\Payment;

interface PaymentInterface
{
    public function startPayment(Order $order);

    public function verify(Payment $payment);
}
