<?php

namespace App\Classes;

use App\Classes\Interfaces\PaymentInterface;
use App\Models\Order;
use App\Models\Payment as PaymentModel;

class Payment
{
    public function __construct(protected PaymentInterface $payment)
    {
    }

    public function start(Order $order)
    {
        return $this->payment->startPayment($order);
    }

    public function verify(PaymentModel $payment)
    {
        return $this->payment->verify($payment);
    }
}
