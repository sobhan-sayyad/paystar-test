<?php

namespace App\Classes;

use App\Exceptions\PaymentStartFaildException;
use App\Models\Order;
use App\Models\Payment;
use Illuminate\Support\Facades\Http;

class PaystarPayment implements Interfaces\PaymentInterface
{
    protected $merchantId = '0yovdk2l6e143';
    protected $merchantKey = '9A3EC03483556C73714510C507529DF70A1228C83477D1455E0511BD72C5AAB8A6715A414AA48B7C905FCEF45868BD26DA58196EF29C77C194C9F14A4B47456CC6454E9D50B388D6FC5AC91BB08B234A8060FDC85B1CEC32CA036DC907F8A4A635D9CBB9CAA31B42549B8D70B2CE5EDE8274FFB55DABFE92D76BC42D91696FAF';
    protected $createUrl = 'https://core.paystar.ir/api/pardakht/create';
    protected $paymentUrl = 'https://core.paystar.ir/api/pardakht/payment';
    protected $verifyUrl = 'https://core.paystar.ir/api/pardakht/verify';

    public function startPayment(Order $order)
    {
        $amount = $order->amount;
        $orderId = $order->id;
        $callBack = route('orders.payment-verify', ['order' => $order->id]);
        $cardNumber = $order->user->verified_card->card_number;

        $sign = hash_hmac(
            'SHA512',
            $amount . '#' . $orderId . '#' . $callBack,
            $this->merchantKey
        );

        $data = [
            'amount'          => $amount,
            'order_id'        => $order->id,
            'callback'        => $callBack,
            'callback_method' => 1,
            'sign'            => $sign,
            'card_number'     => $cardNumber,
        ];

        $headers = [
            'Content-Type'  => 'application/json',
            'Accept'        => 'application/json',
            'Authorization' => 'Bearer ' . $this->merchantId,
        ];

        $payment = Http::withHeaders($headers)->withoutVerifying()->post($this->createUrl, $data);
        $response = json_decode($payment->body());
        if ($response->status != 1) {
            throw new PaymentStartFaildException();
        }

        //Fail previous payments for this order if there is any
        $previousPayments = Payment::where('order_id', $orderId)->update(['status' => Payment::STATUS_FAILED]);

        $paymentData = [
            'order_id'   => $orderId,
            'gateway'    => 'paystar',
            'ref_number' => $response->data->ref_num,
            'status'     => Payment::STATUS_INIT,
        ];
        $paymentRecord = Payment::create($paymentData);

        return [
            'action'     => $this->paymentUrl,
            'method'     => 'POST',
            'parameters' => ['token' => $response->data->token],
        ];
    }

    public function verify(Payment $payment)
    {
        $order = Order::where('id', $payment->order_id)->first();
        $paymentResult = request()->all();
        if (
            $payment->ref_number != $paymentResult['ref_num'] ||
            $payment->order_id != $paymentResult['order_id'] ||
            empty($order)
        ) {
            $paymentAccepted = false;
            $updates = [
                'status'         => Payment::STATUS_FAILED,
                'transaction_id' => $paymentResult['transaction_id'],
                'message'        => 'اطلاعات پرداختی و دیتابیس مغایرت دارد.',
                'card_number'    => $paymentResult['card_number'] ?? null,
                'tracking_code'  => $paymentResult['tracking_code'] ?? null,
            ];
        } elseif ($paymentResult['status'] != 1) {
            $paymentAccepted = false;
            $updates = [
                'status'         => Payment::STATUS_FAILED,
                'transaction_id' => $paymentResult['transaction_id'],
                'message'        => $this->getMessage($paymentResult['status']),
            ];
        } else {
            $sign = hash_hmac(
                'SHA512',
                $order->amount . '#' . $payment->ref_number . '#' . $paymentResult['card_number'] . '#' . $paymentResult['tracking_code'],
                $this->merchantKey
            );
            $data = [
                'amount'  => $order->amount,
                'sign'    => $sign,
                'ref_num' => $payment->ref_number,
            ];

            $headers = [
                'Content-Type'  => 'application/json',
                'Accept'        => 'application/json',
                'Authorization' => 'Bearer ' . $this->merchantId,
            ];

            $verifyPayment = Http::withHeaders($headers)->withoutVerifying()->post($this->verifyUrl, $data);
            if ($verifyPayment['status'] == 1) {
                $paymentAccepted = true;
                $updates = [
                    'status'         => Payment::STATUS_SUCCESS,
                    'transaction_id' => $paymentResult['transaction_id'],
                    'message'        => $this->getMessage($verifyPayment['status']),
                    'card_number'    => $paymentResult['card_number'],
                    'tracking_code'  => $paymentResult['tracking_code'],
                ];

                //make order status payed
                $order->status = Order::STATUS_PAYED;
                $order->payed_at = now();
                $order->save();

            } else {
                $paymentAccepted = false;
                $updates = [
                    'status'         => Payment::STATUS_FAILED,
                    'transaction_id' => $paymentResult['transaction_id'],
                    'message'        => $this->getMessage($verifyPayment['status']),
                    'card_number'    => $paymentResult['card_number'] ?? null,
                    'tracking_code'  => $paymentResult['tracking_code'] ?? null,
                ];
            }
        }

        $payment->update($updates);

        return $paymentAccepted;
    }

    private function getMessage($status)
    {
        $status = (string)$status;
        $message = [
            '1'   => 'موفق',
            '-1'  => 'درخواست نامعتبر (خطا در پارامترهای ورودی)',
            '-2'  => 'درگاه فعال نیست',
            '-3'  => 'توکن تکراری است',
            '-4'  => 'مبلغ بیشتر از سقف مجاز درگاه است',
            '-5'  => 'شناسه ref_num معتبر نیست',
            '-6'  => 'تراکنش قبلا وریفای شده است',
            '-7'  => 'پارامترهای ارسال شده نامعتبر است',
            '-8'  => 'تراکنش را نمیتوان وریفای کرد',
            '-9'  => 'تراکنش وریفای نشد',
            '-98' => 'تراکنش ناموفق',
            '-99' => 'خطای سامانه',
        ];

        return $message[$status] ?? 'خطای نامشخص';
    }
}
