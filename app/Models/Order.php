<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Order extends Model
{
    use HasFactory;

    const STATUS_INIT = 1;
    const STATUS_PAYED = 2;
    const STATUS_FINISHED = 3;
    const STATUS_CANCELED = 4;

    protected $fillable = [
        'user_id',
        'status',
        'amount',
        'description',
        'payed_at',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
