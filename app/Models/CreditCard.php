<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CreditCard extends Model
{
    use HasFactory;

    const STATUS_PENDING = 1;
    const STATUS_VERIFIED = 2;
    const STATUS_REJECTED = 3;

    protected $fillable = [
        'user_id',
        'card_number',
        'status',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
