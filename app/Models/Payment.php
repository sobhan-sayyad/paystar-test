<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    const STATUS_INIT = 1;
    const STATUS_SUCCESS = 2;
    const STATUS_FAILED = 3;

    protected $fillable = [
        'order_id',
        'ref_number',
        'gateway',
        'status',
        'message',
        'transaction_id',
        'card_number',
        'tracking_code',
    ];
}
